<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/20
 * Time: 下午7:44
 */
?>
<!doctype html>
<html>
<head>
    <title>点团队周报系统</title>
    <meta charset="UTF-8">
    <?php $this->load->view('common/css_js');?>
</head>
<body>
<?php $this->load->view('common/header') ?>
<div class="container main-content" style="margin-bottom: 30px;">
    <div class="content">
        <div id="fill-in-week-report">
            <div class="project-information fill-section">
                <h4 class="fill-section-header">项目综述</h4>
                <div class="little-section">
                    <label>1. 总体情况:</label>
                    <p name="general_condition" style="margin-left: 3px; color: #333"><?php if(isset( $report['general_condition'])) echo $report['general_condition']; else echo "无" ?></p>
                </div>
                <div class="little-section">
                    <label>2. 延期情况:</label>
                    <p name="delay_condition" style="margin-left: 3px; color: #333;"><?php if(isset($report['delay_condition'])) echo $report['delay_condition']; else echo "无" ?></p>
                </div>
            </div>
            <div class="activity-discribe fill-section">
                <h4 class="fill-section-header">活动描述</h4>
                <div class="pc-table">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-5">
                                <col class="col-xs-2">
                                <col class="col-xs-2">
                                <col class="col-xs-3">
                            </colgroup>
                            <thead>
                            <tr>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">活动</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">状态描述</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">责任人</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">备注</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $activity_description = $report['activity_description'];
                            $activity_description = explode(";", $activity_description);
                            foreach ($activity_description as $a){
                                $detail = explode(',', $a);
                                ?>
                                <tr>
                                    <th style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php if(isset($detail[0])) echo $detail[0] ?>
                                    </th>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?></td>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?></td>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[3])) echo $detail[3]; else echo "无" ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mobile-list">
                    <div class="little-section">
                        <?php
                        $activity_description = $report['activity_description'];
                        $activity_description = explode(";", $activity_description);
                        $number = [
                            '一', '二', '三', '四', '五', '六',
                        ];
                        foreach ($activity_description as $i => $a){
                            $detail = explode(',', $a);
                        ?>
                            <label><?php echo $i+1; ?>. <?php echo "活动" . $number[$i] ?></label>
                            <div><span>内容:</span>  <?php if(isset($detail[0])) echo $detail[0]; else echo "无" ?></div>
                            <div><span>状态描述:</span>  <?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?></div>
                            <div><span>责任人:</span>  <?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?></div>
                            <div><span>备注:</span>  <?php if(isset($detail[3])) echo $detail[3]; else echo "无" ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="next-week-plan fill-section">
                <h4 class="fill-section-header">下周活动计划</h4>
                <div class="pc-table">
                    <div class="table-responsive">
                        <table class="can-more table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-5">
                                <col class="col-xs-2">
                                <col class="col-xs-2">
                                <col class="col-xs-3">
                            </colgroup>
                            <thead>
                            <tr>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">活动</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">状态描述</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">责任人</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">备注</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $next_week_plan = $report['next_week_plan'];
                            $next_week_plan = explode(";", $next_week_plan);
                            foreach ($next_week_plan as $n){
                                $detail = explode(',', $n);
                                ?>
                                <tr>
                                    <th style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php if(isset($detail[0])) echo $detail[0]; else echo "无" ?>
                                    </th>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?>
                                    </td>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?>
                                    </td>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php if(isset($detail[3])) echo $detail[3]; else echo "无" ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mobile-list">
                    <div class="little-section">
                        <?php
                        $next_week_plan = $report['next_week_plan'];
                        $next_week_plan = explode(";", $next_week_plan);
                        foreach ($next_week_plan as $i => $n){
                            $detail = explode(',', $n);
                            ?>
                            <label><?php echo $i+1; ?>. <?php echo "活动" . $number[$i] ?></label>
                            <div><span>内容:</span>  <?php if(isset($detail[0])) echo $detail[0]; else echo "无" ?></div>
                            <div><span>状态描述:</span>  <?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?></div>
                            <div><span>责任人:</span>  <?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?></div>
                            <div><span>备注:</span>  <?php if(isset($detail[3])) echo $detail[3]; else echo "无" ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="fill-section">
                <h4 class="fill-section-header">项目周例会</h4>
                <div class="form-group">
                    <p><?php if(isset($report['week_meeting'])) echo $report['week_meeting']; else echo "无" ?></p>
                </div>
            </div>
            <div class="problem-track fill-section">
                <h4 class="fill-section-header">项目问题跟踪</h4>
                <div class="pc-table">
                    <div class="table-responsive">
                        <table class="can-more table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-1">
                                <col class="col-xs-5">
                                <col class="col-xs-5">
                            </colgroup>
                            <thead>
                            <tr>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">编号</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">问题描述</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">当前解决方案</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $problem_track = $report['problem_track'];
                            $problem_track = explode(";", $problem_track);
                            foreach ($problem_track as $i => $p){
                                $detail = explode(',', $p);
                                ?>
                                <tr>
                                    <th style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php echo $i ?></th>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php if(isset($detail[0])) echo $detail[0]; else echo "无" ?></td>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mobile-list">
                    <div class="little-section">
                        <?php
                        $problem_track = $report['problem_track'];
                        $problem_track = explode(";", $problem_track);
                        foreach ($problem_track as $i => $p){
                            $detail = explode(',', $p);
                            ?>
                            <label><?php echo $i+1; ?>. <?php echo "问题" . $number[$i] ?></label>
                            <div><span>问题描述:</span>  <?php if(isset($detail[0])) echo $detail[0]; else echo "无" ?></div>
                            <div><span>当前解决方案:</span>  <?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="remark fill-section">
                <h4 class="fill-section-header">备注</h4>
                <div class="form-group">
                    <p name="remark"><?php if(isset($report['remark'])) echo $report['remark'] ?></p>
                </div>
            </div>
            <?php
            $mate_comment = $report['mate_comment'];
            if ($mate_comment != ''){
                ?>
                <div class="fill-section">
                    <h4 class="fill-section-header">组员评价</h4>
                    <div class="pc-table">
                        <div class="table-responsive">
                            <table class="can-more table table-bordered table-striped">
                                <colgroup>
                                    <col class="col-xs-2">
                                    <col class="col-xs-3">
                                    <col class="col-xs-7">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th style="border: 1px solid #ddd;vertical-align: bottom;">组员姓名</th>
                                    <th style="border: 1px solid #ddd;vertical-align: bottom;">投入量</th>
                                    <th style="border: 1px solid #ddd;vertical-align: bottom;">本周工作评价</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $mate_comment = explode(";", $mate_comment);
                                foreach ($mate_comment as $m){
                                    $detail = explode(',', $m);
                                    ?>
                                    <tr>
                                        <th style="border: 1px solid #ddd;vertical-align: bottom">
                                            <?php if(isset($detail[0])) echo $detail[0] ?></th>
                                        <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                            <input type="radio" class="devote" value="3" <?php if ($detail[1] == 3) echo 'checked="checked"' ?> disabled="disabled"><span class="<?php if ($detail[1] == 3) echo 'strong' ?>">超额</span>
                                            <input type="radio" class="devote" value="2" <?php if ($detail[1] == 2) echo 'checked="checked"' ?>  disabled="disabled"><span class="<?php if ($detail[1] == 2) echo 'strong' ?>">基本</span>
                                            <input type="radio" class="devote" value="1" <?php if ($detail[1] == 1) echo 'checked="checked"' ?> disabled="disabled"><span class="<?php if ($detail[1] == 1) echo 'strong' ?>">不足</span>
                                        </td>
                                        <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mobile-list">
                        <table class="can-more table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-2">
                                <col class="col-xs-3">
                                <col class="col-xs-7">
                            </colgroup>
                            <thead>
                            <tr>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">组员姓名</th>
                                <th style="border: 1px solid #ddd;vertical-align: bottom;">投入量</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($mate_comment as $m){
                                $detail = explode(',', $m);
                                ?>
                                <tr>
                                    <th style="border: 1px solid #ddd;vertical-align: bottom">
                                        <?php if(isset($detail[0])) echo $detail[0] ?></th>
                                    <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                        <input type="radio" class="devote" value="3" <?php if ($detail[1] == 3) echo 'checked="checked"' ?> disabled="disabled"><span class="<?php if ($detail[1] == 3) echo 'strong-radio' ?>">超额</span>
                                        <input type="radio" class="devote" value="2" <?php if ($detail[1] == 2) echo 'checked="checked"' ?>  disabled="disabled"><span class="<?php if ($detail[1] == 2) echo 'strong-radio' ?>">基本</span>
                                        <input type="radio" class="devote" value="1" <?php if ($detail[1] == 1) echo 'checked="checked"' ?> disabled="disabled"><span class="<?php if ($detail[1] == 1) echo 'strong-radio' ?>">不足</span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <p style="padding-top: 8px; font-weight: bold;">本周组员工作评价</p>
                        <?php foreach ($mate_comment as $m){
                            $detail = explode(',', $m); ?>
                            <?php if(isset($detail[0])){ ?>
                                <div style="border-bottom: 1px solid #eee; margin-bottom: 5px;">
                                    <span style="font-weight: bold"><?php echo $detail[0] ?>:</span><?php if(isset($detail[2]) && $detail[2] != ' ' ) echo $detail[2]; else echo "无"; ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php $this->load->view('common/render_js') ?>
</body>

