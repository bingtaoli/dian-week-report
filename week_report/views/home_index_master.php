<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/18
 * Time: 16:48
 * 组长页面
 */
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>点团队周报系统</title>
    <?php $this->load->view('common/css_js');?>
</head>
<body>
<?php $this->load->view('common/header') ?>
<div class="container">
    <div class="content">
        <div class="alert alert-warning alert-dismissible" role="alert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="message"></span>
        </div>
        <div class="alert alert-info alert-dismissible" role="alert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="message"></span>
        </div>
        <h2><?php echo $project->project_name ?><?php if ($project->is_reported_this_week){ ?><span>&nbsp;(本周已经提交过周报了)</span><?php } ?></h2>
        <p class="project-intro"><?php echo $project->project_description ?></p>
        <ul class="tab-items">
            <li class="active" id="fill-in-item">填写周报</li>
            <li id="mates-item">增删组员</li>
            <li id="report-history-item">周报提交记录</li>
            <div class="clear"></div>
        </ul>
        <form id="master-report-form">
            <div id="fill-in-week-report">
                <input name="project_name" type="hidden" value="<?php echo $project->project_name ?>">
                <div class="project-information fill-section">
                    <h4 class="fill-section-header">项目综述</h4>
                    <div class="form-group">
                        <label class="control-label" for="general-condition">总体情况</label>
                        <textarea name="general_condition" class="one-row form-control" rows="4"><?php if (isset($temp_report)) echo $temp_report->general_condition ?></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="general-condition">延期情况</label>
                        <textarea name="delay_condition" class="one-row form-control" rows="4"><?php if (isset($temp_report)) echo $temp_report->delay_condition ?></textarea>
                    </div>
                </div>
                <div class="activity-description fill-section">
                    <h4 class="fill-section-header">活动描述</h4>
                    <div class="table-responsive">
                        <table class="can-more table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-5">
                                <col class="col-xs-2">
                                <col class="col-xs-2">
                                <col class="col-xs-3">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>活动</th>
                                <th>状态描述</th>
                                <th>责任人</th>
                                <th>备注</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style="display: none;">
                                <!--activity description prefix ad-->
                                <th><input name="ad_activity" type="text" class="form-control"></th>
                                <td><input name="ad_state" type="text" class="form-control"></td>
                                <td><input name="ad_duty_man" type="text" class="form-control"></td>
                                <td><input name="ad_remark" type="text" class="form-control"></td>
                            </tr>
                            <?php
                            if(isset($temp_report) && isset($temp_report->activity_description) && $temp_report->activity_description != ''){
                                $activity_description = explode(";", $temp_report->activity_description);
                                foreach ($activity_description as $n) {
                                    $detail = explode(',', $n);
                            ?>
                                <tr>
                                    <th><input name="ad_activity" type="text" class="form-control" value="<?php echo $detail[0] ?>"></th>
                                    <td><input name="ad_state" type="text" class="form-control" value="<?php echo $detail[1] ?>"></td>
                                    <td><input name="ad_duty_man" type="text" class="form-control" value="<?php echo $detail[2] ?>"></td>
                                    <td><input name="ad_remark" type="text" class="form-control" value="<?php echo $detail[3] ?>"></td>
                                </tr>
                            <?php } } ?>
                            <tr>
                                <!--activity description prefix ad-->
                                <th><input name="ad_activity" type="text" class="form-control"></th>
                                <td><input name="ad_state" type="text" class="form-control"></td>
                                <td><input name="ad_duty_man" type="text" class="form-control"></td>
                                <td><input name="ad_remark" type="text" class="form-control"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="white-on-green more glyphicon glyphicon-plus" title="增加一项"></div>
                </div>
                <div class="next-week-plan fill-section">
                    <h4 class="fill-section-header">下周活动计划</h4>
                    <div class="table-responsive">
                        <table class="can-more table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-5">
                                <col class="col-xs-2">
                                <col class="col-xs-2">
                                <col class="col-xs-3">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>活动</th>
                                <th>状态描述</th>
                                <th>责任人</th>
                                <th>备注</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!--增加一行的模板-->
                            <tr style="display: none;">
                                <th><input name="np_activity" type="text" class="form-control"></th>
                                <td><input name="np_state" type="text" class="form-control"></td>
                                <td><input name="np_duty_man" type="text" class="form-control"></td>
                                <td><input name="np_remark" type="text" class="form-control"></td>
                            </tr>
                            <?php
                            if(isset($temp_report) && isset($temp_report->next_week_plan) && $temp_report->next_week_plan != ''){
                                $next_week_plan = explode(";", $temp_report->next_week_plan);
                                foreach ($next_week_plan as $n) {
                                    $detail = explode(',', $n);
                                    ?>
                                    <tr>
                                        <th><input name="np_activity" type="text" class="form-control" value="<?php echo $detail[0] ?>"></th>
                                        <td><input name="np_state" type="text" class="form-control" value="<?php echo $detail[1] ?>"></td>
                                        <td><input name="np_duty_man" type="text" class="form-control" value="<?php echo $detail[2] ?>"></td>
                                        <td><input name="np_remark" type="text" class="form-control" value="<?php echo $detail[3] ?>"></td>
                                    </tr>
                            <?php } } ?>
                            <tr>
                                <th><input name="np_activity" type="text" class="form-control"></th>
                                <td><input name="np_state" type="text" class="form-control"></td>
                                <td><input name="np_duty_man" type="text" class="form-control"></td>
                                <td><input name="np_remark" type="text" class="form-control"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="white-on-green more glyphicon glyphicon-plus" title="增加一项"></div>
                </div>
                <div class="fill-section">
                    <h4 class="fill-section-header">项目周例会</h4>
                    <div class="form-group">
                        <textarea name="week_meeting" class="form-control" id="general-condition"><?php if (isset($temp_report)) echo $temp_report->week_meeting ?></textarea>
                    </div>
                </div>
                <div class="problem-track fill-section">
                    <h4 class="fill-section-header">项目问题跟踪</h4>
                    <div class="table-responsive">
                        <table class="can-more table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-5">
                                <col class="col-xs-5">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>问题描述</th>
                                <th>当前解决方案</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style="display: none;">
                                <td><input name="problem_description" type="text" class="form-control"></td>
                                <td><input name="solution" type="text" class="form-control"></td>
                            </tr>
                            <?php
                            if(isset($temp_report) && isset($temp_report->problem_track) && $temp_report->problem_track != ''){
                                $problem_track = explode(";", $temp_report->problem_track);
                                foreach ($problem_track as $p) {
                                    $detail = explode(',', $p);
                                    ?>
                                    <tr>
                                        <th><input name="problem_description" type="text" class="form-control" value="<?php echo $detail[0] ?>"></th>
                                        <td><input name="solution" type="text" class="form-control" value="<?php echo $detail[1] ?>"></td>
                                    </tr>
                            <?php } } ?>
                            <tr>
                                <td><input name="problem_description" type="text" class="form-control"></td>
                                <td><input name="solution" type="text" class="form-control"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="white-on-green more glyphicon glyphicon-plus" title="增加一项"></div>
                </div>
                <div class="mate-comment fill-section">
                    <h4 class="fill-section-header">组员评价</h4>
                    <div class="table-responsive">
                        <table class="can-more table table-bordered table-striped">
                            <colgroup>
                                <col class="col-xs-2">
                                <col class="col-xs-3">
                                <col class="col-xs-7">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>组员姓名</th>
                                <th>投入量</th>
                                <th>本周工作评价</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(isset($temp_report) && isset($temp_report->mate_comment) && $temp_report->mate_comment != '' ){
                                $mate_comment = explode(";", $temp_report->mate_comment);
                                foreach ($mate_comment as $m) {
                                    $detail = explode(',', $m);
                                    $temp_mate_comment[$detail[0]] = [$detail[1], $detail[2]];
                                }
                            }
                            ?>
                            <?php foreach ($mates as $mate) {
                                $mate_name = $mate->mate_name; ?>
                                <tr>
                                    <th><?php echo $mate_name ?></th>
                                    <td style="min-width: 150px;">
                                        <?php if (isset($temp_mate_comment) && isset($temp_mate_comment[$mate_name])) { ?>
                                        <input type="radio" class="devote" value="3" name="devote-<?php echo $mate_name; ?>" <?php if ($temp_mate_comment[$mate_name][0] == 3) echo 'checked="checked"' ?>>超额(3)
                                        <input type="radio" class="devote" value="2" name="devote-<?php echo $mate_name; ?>" <?php if ($temp_mate_comment[$mate_name][0] == 2) echo 'checked="checked"' ?>>基本(2)
                                        <input type="radio" class="devote" value="1" name="devote-<?php echo $mate_name; ?>" <?php if ($temp_mate_comment[$mate_name][0] == 1) echo 'checked="checked"' ?>>不足(1)
                                        <?php } else{ ?>
                                        <input class="devote" name="devote-<?php echo $mate_name; ?>" type="radio" value="3">超额(3)
                                        <input class="devote" name="devote-<?php echo $mate_name; ?>" type="radio" value="2">基本(2)
                                        <input class="devote" name="devote-<?php echo $mate_name; ?>" type="radio" value="1">不足(1)
                                        <?php } ?>
                                    </td>
                                    <?php if (isset($temp_mate_comment) && isset($temp_mate_comment[$mate_name])) { ?>
                                    <td><input name="comment" type="text" class="form-control" value="<?php echo $temp_mate_comment[$mate_name][1] ?>"></td>
                                    <?php } else{ ?>
                                    <td><input name="comment" type="text" class="form-control"></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="fill-section">
                    <h4 class="fill-section-header">备注</h4>
                    <div class="form-group">
                        <textarea name="remark" class="form-control" id="general-condition"><?php if (isset($temp_report)) echo $temp_report->remark ?></textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">提交</button>
                <button id="temp-store" type="button" class="btn btn-default">暂存</button>
                <span id="temp-store-success" class="temp-store-result" style="display: none">暂存成功</span>
                <span id="temp-store-fail" class="temp-store-result" style="color: red; display: none">暂存失败</span>
            </div>
            <div id="add-del-mates" class="none">
                <div class="table-responsive">
                    <table class="can-more table table-bordered">
                        <colgroup>
                            <col class="col-xs-1">
                            <col class="col-xs-2">
                            <col class="col-xs-1">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>组员姓名</th>
                            <th>累计投入量</th>
                            <th class="none"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="border-right: 0px; display: none;">
                            <th><input name="mate_name" type="text" class="form-control"></th>
                            <td><span style="display: inline-block; line-height: 34px;">0</span></td>
                            <td class="add-or-cancel">
                                <span class="glyphicon glyphicon-ok"></span>
                                <span class="glyphicon glyphicon-remove"></span>
                            </td>
                        </tr>
                        <?php foreach ($mates as $mate){ ?>
                        <tr>
                            <th><?php echo $mate->mate_name ?></th>
                            <td><?php echo isset($mate) ? $mate->devote : 0 ; ?></td>
                            <td class="del-item">
                                <span class="glyphicon glyphicon-remove" ></span>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="white-on-green more glyphicon glyphicon-plus" title="增加一项"></div>
            </div>
            <div id="report-history" class="none">
                <div class="table-responsive">
                    <table class="can-more table table-bordered" style="width: 60%;">
                        <colgroup>
                            <col class="col-xs-2">
                            <col class="col-xs-3">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>提交时间</th>
                            <th>链接</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($reports as $r){ ?>
                            <tr>
                                <th><?php echo $r['create_time'] ?></th>
                                <td><a href="<?php echo site_url() . "/report/report_history/" . $r['id']; ?>">查看</a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <div class="table-responsive">
            </div>
        </form>
    </div>
</div>
<div id="report-success-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">发送成功</h4>
            </div>
            <div class="modal-body">
                <p>周报提交成功!!已经邮件通知导师</p>
            </div>
            <div class="modal-footer">
                <button id="finish-report" type="button" class="btn btn-default" data-dismiss="modal">完成</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="del-mate-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除组员确认</h4>
            </div>
            <div class="modal-body">
                <p>确定要删除该组员吗?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="del-mate-confirm-btn" type="button" class="btn btn-primary">确定删除</button>
            </div>
        </div>
    </div>
</div>
<div id="submit-report-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">周报提交确认</h4>
            </div>
            <div class="modal-body">
                <p>确定要提交周报吗?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="submit-confirm-btn" type="button" class="btn btn-primary">确定提交</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#finish-report').on('click', function () {
        open(location, '_self').close();
    });
</script>

<?php $this->load->view('common/footer') ?>
<?php $this->load->view('common/render_js') ?>
<?php $this->load->view('common/render_report_js') ?>

</body>