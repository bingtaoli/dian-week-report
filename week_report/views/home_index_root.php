<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/20
 * Time: 11:13
 */
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>点团队周报系统</title>
    <?php $this->load->view('common/css_js');?>
</head>
<body>
<?php $this->load->view('common/header') ?>

<div class="container">
    <div class="content">
        <div class="alert alert-warning alert-dismissible" role="alert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="message"></span>
        </div>
        <div class="alert alert-info alert-dismissible" role="alert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="message"></span>
        </div>
        <ul class="tab-items">
            <li class="active" id="projects-item">管理项目组</li>
            <li id="senders-item">提醒人邮箱</li>
            <div class="clear"></div>
        </ul>
        <form>
            <div id="manage-project">
                <table class="can-more table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-2">
                        <col class="col-xs-1">
                        <col class="col-xs-2">
                        <col class="col-xs-2">
                        <col class="col-xs-1">
                        <col class="col-xs-1">
                        <col class="col-xs-1">
                        <col class="col-xs-2">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>项目组名称</th>
                        <th>组长</th>
                        <th>组长喻信ID</th>
                        <th>组长邮箱</th>
                        <th>项目启动时间</th>
                        <th>项目计划结束时间</th>
                        <th>本周是否提交周报</th>
                        <th class="none"></th>
                        <th class="none"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="border-right: 0px; display: none;">
                        <td name="project_name"></td>
                        <td name="project_master"></td>
                        <td name="project_master_yx"></td>
                        <td name="project_master_email"></td>
                        <td name="start_time"></td>
                        <td name="plan_end_time"></td>
                        <td>否</td>
                        <td class="de-item" style="border: none;border-right: solid 1px #fff;border-bottom: solid 1px #fff;background: #fff;">
                            <span class="glyphicon glyphicon-edit" style="display: inline-block;line-height: 34px;width: 24px;cursor: pointer;"></span>
                            <span class="glyphicon glyphicon-remove" style="display: inline-block;line-height: 34px;width: 24px;cursor: pointer;"></span>
                        </td>
                        <td name="project_description" style="display: none;"></td>
                    </tr>
                    <?php foreach($projects as $p){ ?>
                    <tr>
                        <td name="project_name"><?php echo $p['project_name'] ?></td>
                        <td name="project_master"><?php echo $p['project_master'] ?></td>
                        <td name="project_master_yx"><?php if(isset($p['project_master_yx'])) echo $p['project_master_yx'] ?></td>
                        <td name="project_master_email"><?php if(isset($p['project_master_email'])) echo $p['project_master_email'] ?></td>
                        <td name="start_time"><?php echo $p['start_time'] ?></td>
                        <td name="plan_end_time"><?php echo $p['plan_end_time'] ?></td>
                        <td ><?php if ($p['is_reported_this_week']) echo "是"; else echo "否"; ?></td>
                        <td class="del-item">
                            <span class="glyphicon glyphicon-edit"></span>
                            <span class="glyphicon glyphicon-remove"></span>
                        </td>
                        <td name="project_description" style="display: none"><?php echo $p['project_description'] ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div id="add-project-more" class="white-on-green glyphicon glyphicon-plus" title="增加一项"></div>
            </div>
            <div id="receiver-email" class="none">
                <div style="margin-bottom: 8px;">
                    <span>是否发送邮件:&nbsp;</span>
                    <div id="on-off-container" style="display: inline-block;">
                        <input id="on-off" type="checkbox" data-size="mini" <?php if ($on_off) {echo "checked";} ?> >
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="can-more table table-bordered table-striped">
                        <colgroup>
                            <col class="col-xs-3">
                            <col class="col-xs-4">
                            <col class="col-xs-1">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>名称</th>
                            <th>邮箱</th>
                            <th class="none"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="display: none;">
                            <td><input name="receiver_name" type="text" class="form-control"></td>
                            <td><input name="email" type="text" class="form-control"></td>
                            <td class="add-or-cancel">
                                <span class="glyphicon glyphicon-ok"></span>
                                <span class="glyphicon glyphicon-remove"></span>
                            </td>
                        </tr>
                        <?php foreach($receivers as $r ){ ?>
                        <tr>
                            <td><?php echo $r['receiver_name'] ?></td>
                            <td><?php echo $r['email'] ?></td>
                            <td class="del-item">
                                <span class="glyphicon glyphicon-remove"></span>
                            </td>
                        </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
                <div class="white-on-green more glyphicon glyphicon-plus" title="增加一项"></div>
            </div>
        </form>
    </div>
</div>
<div id="del-project-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>确定要删除该项目吗?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="del-project-confirm-btn" type="button" class="btn btn-primary">确定删除</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="add-project-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">增加项目</h4>
            </div>
            <div class="modal-body" style="width: 80%; margin: 0 auto;">
                <div class="form-group">
                    <label>项目名称</label>
                    <input name="project_name" type="text" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label>项目组长</label>
                    <input name="project_master" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目组长喻信ID</label>
                    <input name="project_master_yx" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目组长喻信email</label>
                    <input name="project_master_email" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目简介</label>
                    <input name="project_description" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目开始时间</label>
                    <input name="start_time" type="text" class="form-control" placeholder="y-m-d">
                </div>
                <div class="form-group">
                    <label>项目计划结束时间</label>
                    <input name="plan_end_time" type="text" class="form-control" placeholder="y-m-d">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="add-project-confirm-btn" type="button" class="btn btn-primary">确定增加</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="edit-project-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">修改项目信息</h4>
            </div>
            <div class="modal-body" style="width: 80%; margin: 0 auto;">
                <div class="form-group">
                    <label>项目名称</label>
                    <input name="project_name" type="text" class="form-control" disabled="disabled">
                </div>
                <div class="form-group">
                    <label>项目组长</label>
                    <input name="project_master" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目组长喻信ID</label>
                    <input name="project_master_yx" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目组长喻信email</label>
                    <input name="project_master_email" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目简介</label>
                    <input name="project_description" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>项目开始时间</label>
                    <input name="start_time" type="text" class="form-control" placeholder="y-m-d">
                </div>
                <div class="form-group">
                    <label>项目计划结束时间</label>
                    <input name="plan_end_time" type="text" class="form-control" placeholder="y-m-d">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="edit-project-confirm-btn" type="button" class="btn btn-primary">确定修改</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="del-receiver-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>确定要删除该收件人吗?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="del-receiver-confirm-btn" type="button" class="btn btn-primary">确定删除</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $this->load->view('common/footer') ?>
<?php $this->load->view('common/render_js') ?>
<?php $this->load->view('common/render_project_js') ?>
<?php $this->load->view('common/render_email_js') ?>

<script>
    window.has_on = false;
    <?php if ($on_off) {echo "window.has_on = true";} ?>
</script>
</body>