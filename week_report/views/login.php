<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/18
 * Time: 21:54
 * 登录界面
 */
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>点团队周报系统</title>
    <?php $this->load->view('common/css_js');?>
</head>
<body>
<div class="admin-form">
    <div class="container">
        <div class="row">
            <div class="login-wrap" style="margin: 50px auto 0; width: 70%; max-width: 650px; min-height: 320px;">
                <div class="alert alert-warning alert-dismissible" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="message"></span>
                </div>
                <div class="widget">
                    <div class="widget-head">登录</div>
                    <div class="widget-content" style="min-height: 270px;">
                        <div class="pad">
                            <form id="login-form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-lg-3">用户名</label>
                                    <div class="col-lg-9">
                                        <input name="username" type="text" class="form-control">
                                    </div>
                                </div>
                                <!-- Password -->
                                <div class="form-group">
                                    <label class="control-label col-lg-3">密码</label>
                                    <div class="col-lg-9">
                                        <input name="password" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <div class="checkbox">
                                            <label>
                                                <input name="remember_me" type="checkbox">记住我
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-lg-offset-2">
                                    <button type="submit" class="btn btn-danger">登录</button>
                                </div>
                                <br/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('common/footer') ?>
<?php $this->load->view('common/render_js') ?>
</body>
