<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/24
 * Time: 15:04
 */

/**
 *  说明: 由于在QQ邮箱测试时出现了 ">>" 被转义成了 &gt;&gt; 所以不使用::after来为每一项加上后缀">>"
 */

?>
<!doctype html>
<html>
<head>
    <title>index</title>
    <meta charset="UTF-8">
    <?php $this->load->view('email/email_css') ?>
</head>
<body>
<div class="container">
    <p>建议在浏览器中打开查看,<a href="<?php echo site_url(); ?>/report/report_history/<?php echo $report['id']; ?>">周报网页链接</a></p>
    <div class="header">
        <!-- TODO 变成域名 -->
        <div class="fl dian-logo">
            <img src="http://202.114.20.78<?php if(DIR_IN_ROOT){echo '/' . DIR_IN_ROOT;}?>/public/images/dian.jpg"  width="102px;" height="42px;">
            <span style="font-weight: bold;line-height: 37px;padding-top: 5px;">&nbsp;|&nbsp;周报系统</span>
        </div>
        <div class="clear"></div>
    </div>
    <div class="content">
        <h2><?php if(isset($project->project_name)) echo $project->project_name ?></h2>
        <p class="group-intro"><?php echo $project->project_description ?></p>
        <div id="fill-in-week-report">
            <div class="project-information fill-section">
                <h4 class="fill-section-header" style="font-size: 16px; margin: 0; padding: 8px 0;">项目综述&gt;&gt;</h4>
                <div class="little-section">
                    <label>1. 总体情况:</label>
                    <p name="general_condition" style="margin-left: 3px; color: #333"><?php if(isset( $report['general_condition'])) echo $report['general_condition']; else echo "无" ?></p>
                </div>
                <div class="little-section">
                    <label>2. 延期情况:</label>
                    <p name="delay_condition" style="margin-left: 3px; color: #333;"><?php if(isset($report['delay_condition'])) echo $report['delay_condition']; else echo "无" ?></p>
                </div>
            </div>
            <div class="activity-discribe fill-section">
                <h4 class="fill-section-header" style="font-size: 16px; margin: 0; padding: 8px 0;">活动描述&gt;&gt;</h4>
                <table class="table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-5">
                        <col class="col-xs-2">
                        <col class="col-xs-2">
                        <col class="col-xs-3">
                    </colgroup>
                    <thead>
                    <tr>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">活动</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom; min-width: 60px;">状态描述</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom; min-width: 50px;">责任人</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">备注</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $activity_description = $report['activity_description'];
                    $activity_description = explode(";", $activity_description);
                    foreach ($activity_description as $a){
                        $detail = explode(',', $a);
                    ?>
                    <tr>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[0])) echo $detail[0] ?></th>
                        <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?></td>
                        <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?></td>
                        <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[3])) echo $detail[3]; else echo "无" ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="next-week-plan fill-section">
                <h4 class="fill-section-header" style="font-size: 16px; margin: 0; padding: 8px 0;">下周活动计划&gt;&gt;</h4>
                <table class="can-more table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-5">
                        <col class="col-xs-2">
                        <col class="col-xs-2">
                        <col class="col-xs-3">
                    </colgroup>
                    <thead>
                    <tr>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">活动</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom; min-width: 60px;">状态描述</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom; min-width: 50px;">责任人</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">备注</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $next_week_plan = $report['next_week_plan'];
                    $next_week_plan = explode(";", $next_week_plan);
                    foreach ($next_week_plan as $n){
                        $detail = explode(',', $n);
                        ?>
                        <tr>
                            <th style="border: 1px solid #ddd;vertical-align: bottom;">
                                <?php if(isset($detail[0])) echo $detail[0]; else echo "无" ?>
                            </th>
                            <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                <?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?>
                            </td>
                            <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                <?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?>
                            </td>
                            <td style="border: 1px solid #ddd;vertical-align: bottom;">
                                <?php if(isset($detail[3])) echo $detail[3]; else echo "无" ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="fill-section">
                <h4 class="fill-section-header" style="font-size: 16px; margin: 0; padding: 8px 0;">项目周例会&gt;&gt;</h4>
                <div class="form-group">
                    <p><?php if(isset($report['week_meeting'])) echo $report['week_meeting']; else echo "无" ?></p>
                </div>
            </div>
            <div class="problem-track fill-section">
                <h4 class="fill-section-header" style="font-size: 16px; margin: 0; padding: 8px 0;">项目问题跟踪&gt;&gt;</h4>
                <table class="can-more table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-1">
                        <col class="col-xs-5">
                        <col class="col-xs-5">
                    </colgroup>
                    <thead>
                    <tr>
                        <th style="border: 1px solid #ddd;vertical-align: bottom; min-width: 35px;">编号</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">问题描述</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">当前解决方案</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $problem_track = $report['problem_track'];
                    $problem_track = explode(";", $problem_track);
                    foreach ($problem_track as $i => $p){
                        $detail = explode(',', $p);
                        ?>
                    <tr>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">
                            <?php echo $i ?></th>
                        <td style="border: 1px solid #ddd;vertical-align: bottom;">
                            <?php if(isset($detail[0])) echo $detail[0]; else echo "无" ?></td>
                        <td style="border: 1px solid #ddd;vertical-align: bottom;">
                            <?php if(isset($detail[1])) echo $detail[1]; else echo "无" ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="remark fill-section">
                <h4 class="fill-section-header" style="font-size: 16px; margin: 0; padding: 8px 0;">备注&gt;&gt;</h4>
                <div class="form-group">
                    <p name="remark"><?php if(isset($report['remark'])) echo $report['remark'] ?></p>
                </div>
            </div>
            <?php
            $mate_comment = $report['mate_comment'];
            if ($mate_comment != ''){
            ?>
            <div class="fill-section">
                <h4 class="fill-section-header" style="font-size: 16px; margin: 0; padding: 8px 0;">组员评价&gt;&gt;</h4>
                <table class="can-more table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-2">
                        <col class="col-xs-3">
                        <col class="col-xs-7">
                    </colgroup>
                    <thead>
                    <tr>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">组员姓名</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom; min-width: 140px;">投入量</th>
                        <th style="border: 1px solid #ddd;vertical-align: bottom;">本周工作评价</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $mate_comment = explode(";", $mate_comment);
                    foreach ($mate_comment as $m){
                        $detail = explode(',', $m);
                        ?>
                    <tr>
                        <th style="border: 1px solid #ddd;vertical-align: bottom">
                            <?php if(isset($detail[0])) echo $detail[0] ?></th>
                        <td style="border: 1px solid #ddd;vertical-align: bottom;">
                            <input type="radio" class="devote" value="3" <?php if ($detail[1] == 3) echo 'checked="checked"' ?> disabled="disabled">超额
                            <input type="radio" class="devote" value="2" <?php if ($detail[1] == 2) echo 'checked="checked"' ?>  disabled="disabled">基本
                            <input type="radio" class="devote" value="1" <?php if ($detail[1] == 1) echo 'checked="checked"' ?> disabled="disabled">不足
                        </td>
                        <td style="border: 1px solid #ddd;vertical-align: bottom;"><?php if(isset($detail[2])) echo $detail[2]; else echo "无" ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <?php } ?>
        </div>
        <p>
            <?php if(isset($project->project_master_email)){ ?>
            <a href="mailto:<?php echo $project->project_master_email ?>">给该项目组长反馈</a>
            <?php } ?>
        </p>
    </div>
</body>
