<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/28
 * Time: 13:32
 */
?>
<style type="text/css">
    body {
        font-family: Microsoft Yahei;
        font-style: normal;
        position: relative;
    }
    p {
        margin: 6px 0;
    }
    .container {
        width: 75%;
        background: #fff;
        margin: 0 auto;
    }
    .fl {
        float: left;
    }
    .header {
        padding: 10px 15px;
        border-bottom: 2px solid #f3f5f7;
    }
    .identify span {
        font-weight: bold;
    }
    .group-intro {
        color: #7b858f;
    }
    h2 span {
        font-size: 14px;
    }
    ul {
        list-style: none;
        padding: 0;
    }
    ul li {
        float: left;
        width: 100px;
        margin-right: 20px;
        padding-left: 5px;
        padding-bottom: 8px;
        padding-top: 8px;
        font-size: 16px;
        font-weight: bold;
        color: #a9b0b6;
        cursor: pointer;
    }
    ul li:hover {
        border-bottom: 3px solid #56cbca;
    }
    .clear {
        clear: both;
    }
    .fill-section-header {
        font-weight: bold;
        padding: 8px 0;
    }
    form label {
        color: #c1c1c1;
    }
    .dian-logo-intro {
        font-weight: bold;
        line-height: 37px;
        padding-top: 5px;
    }
    table.can-more {
        margin-bottom: 2px;
    }
    .little-section {
        margin-bottom: 12px;
    }
    /* bootstrap style */
    .table {
        border-collapse: collapse !important;
    }
    .table td,
    .table th {
        background-color: #fff !important;
    }
    .table-bordered {
        border: 1px solid #ddd;
    }
    .table-bordered th,
    .table-bordered td {
        backgroundorder: 1px solid #ddd !important;
    }
    table {
        background-color: transparent;
    }
    caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #777;
        text-align: left;
    }
    th {
        text-align: left;
    }
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }
    .table > thead > tr > th,
    .table > tbody > tr > th,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > tbody > tr > td,
    .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        border-top: 1px solid #ddd;
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
    }
    .table > caption + thead > tr:first-child > th,
    .table > colgroup + thead > tr:first-child > th,
    .table > thead:first-child > tr:first-child > th,
    .table > caption + thead > tr:first-child > td,
    .table > colgroup + thead > tr:first-child > td,
    .table > thead:first-child > tr:first-child > td {
        border-top: 0;
    }
    .table > tbody + tbody {
        border-top: 2px solid #ddd;
    }
    .table .table {
        background-color: #fff;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
    }
</style>
