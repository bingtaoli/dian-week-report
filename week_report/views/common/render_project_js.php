<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/12
 * Time: 下午10:08
 */
?>
<script>
    function get_project_information(elem){
        var project_name = $(elem).find('[name="project_name"]').val();
        var project_master = $(elem).find('[name="project_master"]').val();
        var project_master_yx = $(elem).find('[name="project_master_yx"]').val();
        var project_master_email = $(elem).find('[name="project_master_email"]').val();
        var project_description = $(elem).find('[name="project_description"]').val();
        var start_time = $(elem).find('[name="start_time"]').val();
        var plan_end_time = $(elem).find('[name="plan_end_time"]').val();
        var data = {
            'project_name': project_name,
            'project_master': project_master,
            'project_master_yx': project_master_yx,
            'project_master_email': project_master_email,
            'project_description': project_description,
            'start_time': start_time,
            'plan_end_time': plan_end_time
        };
        return data;
    }
    function set_project_information(tr, data){
        $(tr).find('[name="project_name"]').text(data.project_name);
        $(tr).find('[name="project_master"]').text(data.project_master);
        $(tr).find('[name="project_master_yx"]').text(data.project_master_yx);
        $(tr).find('[name="project_master_email"]').text(data.project_master_email);
        $(tr).find('[name="start_time"]').text(data.start_time);
        $(tr).find('[name="plan_end_time"]').text(data.plan_end_time);
    }
    /**
     * 增项目触发弹窗
     */
    $('#add-project-more').on('click', function(){
        $('#add-project-modal').modal();
    });
    $('#add-project-confirm-btn').on('click', function(){
        var modal = $(this).parents('.modal');
        var data = get_project_information(modal);
        console.log(data);
        var url = "<?=site_url()?>/project/add_project";
        _ajax_send(data, url, add_project_success_cb, add_project_fail_cb, data);
        $('#add-project-modal').modal('hide');
    });
    /**
     * 删除按钮触发弹窗
     */
    $('#manage-project').on('click', '.del-item .glyphicon-remove', function(){
        add_pre_del_mark(this);
        $('#del-project-modal').modal();
    });
    /**
     * 删除项目模态框确定
     * pre-del为存放项目名称的那个td
     */
    $('#del-project-confirm-btn').on('click', function(){
        var elem = $('.pre-del');
        var project_name = $('.pre-del').text();
        var url = "<?=site_url()?>/project/del_project";
        var data = {
            'project_name': project_name
        };
        console.log(project_name);
        _ajax_send(data, url, del_project_success_cb, del_project_fail_cb, elem);
        $('#del-project-modal').modal('hide');
    });
    /**
     * edit触发弹窗
     */
    $('#manage-project').on('click', '.del-item .glyphicon-edit', function(){
        var tr = $(this).parents('tr');
        $('.editing').removeClass('editing');
        $(tr).addClass('editing');
        var project_name = $(tr).find('[name="project_name"]').text();
        var project_master = $(tr).find('[name="project_master"]').text();
        var project_master_yx = $(tr).find('[name="project_master_yx"]').text();
        var project_master_email = $(tr).find('[name="project_master_email"]').text();
        var project_description = $(tr).find('[name="project_description"]').text();
        var start_time = $(tr).find('[name="start_time"]').text();
        var plan_end_time = $(tr).find('[name="plan_end_time"]').text();

        var modal = $('#edit-project-modal');
        $(modal).find('[name="project_name"]').val(project_name);
        $(modal).find('[name="project_master"]').val(project_master);
        $(modal).find('[name="project_master_yx"]').val(project_master_yx);
        $(modal).find('[name="project_master_email"]').val(project_master_email);
        $(modal).find('[name="project_description"]').val(project_description);
        $(modal).find('[name="start_time"]').val(start_time);
        $(modal).find('[name="plan_end_time"]').val(plan_end_time);
        $(modal).modal();
    });
    $('#edit-project-confirm-btn').on('click', function(){
        var modal = $(this).parents('.modal');
        var url = "<?=site_url()?>/project/update_project";
        var data = get_project_information(modal);
        console.log(data);
        var tr = $('.editing');
        var arr = [tr, data];
        _ajax_send(data, url, update_project_success_cb, update_project_fail_cb, arr);
        $('#edit-project-modal').modal('hide');
    });
</script>