<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/13
 * Time: 上午10:12
 */
?>
<script>
    /**
     *  打开或者关闭邮件提醒
     */
    $('#on-off').on('click', function(){
        console.log("hello");
        /**
         * 判断设置的标志位
         */
        var now_on = window.has_on;
        if (now_on){
            //如果现在是打开的,则关闭
            var url = "<?=site_url()?>/report/off_email";
            _ajax_send(null, url, turn_off_email_success_cb, turn_off_email_fail_cb);
        } else {
            //打开
            var url = "<?=site_url()?>/report/on_email";
            _ajax_send(null, url, turn_on_email_success_cb, turn_on_email_fail_cb);
        }
    });

    /**
     * 增收件人
     */
    $('#receiver-email').on('click', '.glyphicon-ok', function(){
        var tr = $(this).parents('tr');
        var receiver_name = $(tr).find('[name="receiver_name"]').val();
        var email = $(tr).find('[name="email"]').val();
        var url = "<?=site_url()?>/report/add_receiver";
        var data = {
            'receiver_name': receiver_name,
            'email': email
        };
        _ajax_send(data, url, add_receiver_success_cb, add_receiver_fail_cb, this);
    });
    $('#receiver-email').on('click', '.add-or-cancel .glyphicon-remove', function(){
        $(this).parents('tr').remove();
    });
    /**
     * 删收件人按钮触发弹窗
     */
    $('#receiver-email').on('click', '.del-item .glyphicon-remove', function(){
        add_pre_del_mark(this);
        $('#del-receiver-modal').modal();
    });
    /**
     * 删除收件人模态框确定
     */
    $('#del-receiver-confirm-btn').on('click', function(){
        var elem = $('.pre-del');
        var receiver_name = $('.pre-del').text();
        var url = "<?=site_url()?>/report/del_receiver";
        var data = {
            'receiver_name': receiver_name
        };
        _ajax_send(data, url, del_receiver_success_cb, del_receiver_fail_cb, elem);
        $('#del-receiver-modal').modal('hide');
    });
</script>
