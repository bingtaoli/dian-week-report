<?php
/**
 * Created by PhpStorm.
 * User: bingtao
 * Date: 2015/12/3
 * Time: 11:13
 */
?>
<script>
    /**
     * 登录提交
     */
    $('#login-form').on('submit', function (e) {
        e.preventDefault();
        var data = {
            'username':  $('[name="username"]').val(),
            'password':  $('[name="password"]').val(),
            'remember_me': $('[name="remember_me"]:checked').length == 1 ? 1 : 0
        };
        $.ajax({
            type: "POST",
            url: "<?=site_url()?>/user/login",
            dataType: 'json',
            data: data,
            success: function(json){
                if (json.status == "success") {
                    window.location.href = "<?php echo site_url(); ?>/home";
                } else if (json.status === 'error') {
                    login_fail_cb(json.message);
                }
            },
            error: function (e) {
                login_fail_cb(e.message);
            }
        });
    });
    function login_fail_cb(message){
        /**
         *  把原来的hide
         * */
        $('.alert').hide();
        var warning = $('.alert-warning')[0];
        var clone_warning = $(warning).clone();
        $(clone_warning).find('.message').text(message);
        $('.login-wrap').prepend(clone_warning);
        $(clone_warning).show();
        //该信息在1s后自动隐藏
        setTimeout(function(){
            $('.alert').hide();
        }, 1000);
    }
    /**
     * 登出
     */
    $('.logout').on('click', function(){
        var url = "<?=site_url()?>/user/logout";
        _ajax_send(null, url, logout_success, null);
    });
    function logout_success(){
        window.location.href = "<?php echo site_url(); ?>/home";
    };

    /**
     * 增组员
     */
    $('#add-del-mates').on('click', '.glyphicon-ok', function(){
        var mate_name = $(this).parent().siblings().eq(0).find('input').val();
        var project_name = $('[name="project_name"]').val();
        var url = "<?=site_url()?>/mate/add_mate";
        var data = {
            'mate_name': mate_name,
            'project_name': project_name
        };
        _ajax_send(data, url, add_mate_success_cb, add_mate_fail_cb, this);
    });
    /**
     * 删组员按钮触发弹窗
     */
    $('#add-del-mates').on('click', '.del-item .glyphicon-remove', function(){
        add_pre_del_mark(this);
        $('#del-mate-modal').modal();
    });
    $('#add-del-mates').on('click', '.add-or-cancel .glyphicon-remove', function(){
        $(this).parents('tr').remove();
    });
    /**
     * 删除组员模态框确定
     */
    $('#del-mate-confirm-btn').on('click', function(){
        var elem = $('.pre-del');
        var mate_name = $('.pre-del').text();
        var url = "<?=site_url()?>/mate/del_mate";
        var data = {
            'mate_name': mate_name,
        };
        _ajax_send(data, url, del_mate_success_cb, del_mate_fail_cb, elem);
        $('#del-mate-modal').modal('hide'); 
    });

</script>
