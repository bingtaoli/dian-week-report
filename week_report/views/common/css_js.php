<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/9/19
 * Time: 16:01
 */
?>
<link rel="stylesheet" type="text/css" href="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT;}?>/public/third_part/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT;}?>/public/third_part/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT;}?>/public/styles/dian.css" />

<script type="text/javascript" src="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT ; }?>/public/third_part/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT ; }?>/public/third_part/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT ; }?>/public/scripts/dian.js"></script>
<script type="text/javascript" src="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT ; }?>/public/scripts/callback.js"></script>

<link rel="shortcut icon" href="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT ; }?>/public/images/dian_square.jpg" >

<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,user-scalable=no">