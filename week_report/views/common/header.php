<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/21
 * Time: 21:46
 */
?>
<header>
    <div class="container">
        <div class="fl dian-logo"><img src="<?php if (DIR_IN_ROOT) {echo '/' . DIR_IN_ROOT;}?>/public/images/dian.gif" style="max-height: 40px; width: auto;"></div>
        <div class="fl dian-logo-intro">&nbsp;|&nbsp;周报系统</div>
        <?php if(isset($username)){ ?>
            <div class="fr" title="登出"><span class="logout white-on-green glyphicon glyphicon-log-out"></span></div>
            <div class="fr split">/</div>
            <div class="fr identify"><span><?php echo "Hi, " . $username ?></span></div>
        <?php } ?>
        <div class="clear"></div>
    </div>
</header>
