<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/12
 * Time: 下午10:10
 */
?>
<script>
    /**
     * 组长页面的提交周报
     */
    $('#master-report-form').on('submit', function(e){
        e.preventDefault();
        $('#submit-report-modal').modal();
    });
    $('#submit-confirm-btn').on('click', function(){
        var url = "<?=site_url()?>/report/add_report";
        var data = _get_master_report_data();
        _ajax_send(data, url, report_success_cb, report_fail_cb);
        $('#submit-report-modal').modal('hide');
    });

    /**
     * 暂存功能
     */
    $('#temp-store').on('click', function(e){
        e.preventDefault();
        var url = "<?=site_url()?>/report/add_temp_report";
        var data = _get_master_report_data();
        _ajax_send(data, url, temp_store_success_cb, temp_store_fail_cb);
    });

    function _get_master_report_data(){
        var general_condition = $('[name="general_condition"]').val();
        var delay_condition = $('[name="delay_condition"]').val();
        var project_name = $('[name="project_name"]').val();
        var remark = $('[name="remark"]').val();
        /**
         * 活动描述,多个活动使用分号分隔
         */
        var trs = $('.activity-description table tr:visible');
        var activity_description = [];
        for (var i = 1; i < trs.length; i++) {
            var tr = trs[i];
            var ad_activity = $(tr).find('[name="ad_activity"]').val() ? $(tr).find('[name="ad_activity"]').val() : " ";
            if (ad_activity == " ") {
                break;
            }
            var ad_state = $(tr).find('[name="ad_state"]').val() ? $(tr).find('[name="ad_state"]').val() : " ";
            var ad_duty_man = $(tr).find('[name="ad_duty_man"]').val() ? $(tr).find('[name="ad_duty_man"]').val() : " ";
            var ad_remark = $(tr).find('[name="ad_remark"]').val() ? $(tr).find('[name="ad_remark"]').val() : " ";
            var temp_arr = [];
            temp_arr.push(ad_activity);
            temp_arr.push(ad_state);
            temp_arr.push(ad_duty_man);
            temp_arr.push(ad_remark);
            var temp_str = temp_arr.join(',');
            activity_description[i-1] = temp_str;
        }
        activity_description = activity_description.length > 0 ? activity_description.join(';') : "";
        /**
         * 下周活动计划
         */
        var trs = $('.next-week-plan table tr:visible');
        var next_week_plan = [];
        for (var i = 1; i < trs.length; i++) {
            var tr = trs[i];
            var np_activity = $(tr).find('[name="np_activity"]').val() ? $(tr).find('[name="np_activity"]').val() : " ";
            if (np_activity == " ") {
                break;
            }
            var np_state = $(tr).find('[name="np_state"]').val() ? $(tr).find('[name="np_state"]').val() : " ";
            var np_duty_man = $(tr).find('[name="np_duty_man"]').val() ? $(tr).find('[name="np_duty_man"]').val() : " ";
            var np_remark = $(tr).find('[name="np_remark"]').val() ? $(tr).find('[name="np_remark"]').val() : " ";
            var temp_arr = [];
            temp_arr.push(np_activity);
            temp_arr.push(np_state);
            temp_arr.push(np_duty_man);
            temp_arr.push(np_remark);
            var temp_str = temp_arr.join(',');
            next_week_plan[i-1] = temp_str;
        }
        next_week_plan = next_week_plan.length > 0 ? next_week_plan.join(';') : "";
        /**
         * 项目周例会
         */
        var week_meeting = $('[name="week_meeting"]').val();
        /**
         * 项目问题跟踪
         */
        var trs = $('.problem-track table tr:visible');
        var problem_track = [];
        for (var i = 1; i < trs.length; i++) {
            var tr = trs[i];
            var problem_description = $(tr).find('[name="problem_description"]').val() ? $(tr).find('[name="problem_description"]').val() : " ";
            if (problem_description == " ") {
                break;
            }
            var solution = $(tr).find('[name="solution"]').val() ? $(tr).find('[name="solution"]').val() : " ";
            var temp_arr = [];
            temp_arr.push(problem_description);
            temp_arr.push(solution);
            var temp_str = temp_arr.join(',');
            problem_track[i-1] = temp_str;
        }
        problem_track = problem_track.length > 0 ? problem_track.join(';') : "";
        /**
         * 组员评价
         */
        var trs = $('.mate-comment table tr:visible');
        var mate_comment = [];
        for (var i = 1; i < trs.length; i++) {
            var tr = trs[i];
            var mate_name = $($(tr).find('th')[0]).text() ? $($(tr).find('th')[0]).text() : " ";
            if (mate_name == " "){
                break;
            }
            var devote = $(tr).find('[class="devote"]:checked').val() ? $(tr).find('[class="devote"]:checked').val() : 4; //如果没有val则设置一个无效值
            var comment = $(tr).find('[name="comment"]').val() ? $(tr).find('[name="comment"]').val() : ' ';
            var temp_arr = [];
            temp_arr.push(mate_name);
            temp_arr.push(devote);
            temp_arr.push(comment);
            var temp_str = temp_arr.join(',');
            mate_comment[i-1] = temp_str;
        }
        mate_comment = mate_comment.length > 0 ? mate_comment.join(';') : "";
        /**
         * 构建ajax传递数组
         */
        var data = {
            'general_condition': general_condition,
            'delay_condition': delay_condition,
            'activity_description': activity_description,
            'next_week_plan': next_week_plan,
            'week_meeting': week_meeting,
            'problem_track': problem_track,
            'mate_comment':  mate_comment,
            'remark': remark,
            'project_name' : project_name,
        };
        return data;
    }
</script>
