<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/21
 * Time: 15:13
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_project extends CI_Model
{

    var $db;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $db
     * not via magic method, so pass $db
     */
    public function init($db)
    {
        $this->db = $db;
    }

    public function add_project($data){
        $this->db->insert('project', $data);
    }

    public function del_project($project_name){
        try {
            $this->db->where('project_name', $project_name);
            $this->db->delete('project');
        } catch (Exception $e){
            throw new Exception($e);
        }
    }

    public function update_project($data){
        try {
            $origin_data = $this->get_project_by_name($data['project_name']);
            if ($origin_data == null) {
                throw new Exception("没有该项目");
            }
            $data = array_merge($origin_data, $data);
            $this->db->where('project_name', $data['project_name']);
            $this->db->update('project', $data);
        } catch (Exception $e){
            throw new Exception($e);
        }
    }

    /**
     * @param $username
     * @return null
     * 根据组长名称获取项目
     */
    public function get_project_by_master($username){
        $this->db->where('project_master', $username);
        $result = $this->db->get('project')->result();
        if ($result){
            /**
             * result()是一个数组,每个元素都是一个stdClass
             */
            return $result[0];
        } else {
            return NULL;
        }
    }

    public function get_project_by_master_yx($yxid){
        $this->db->where('project_master_yx', $yxid);
        $result = $this->db->get('project')->result_array();
        if ($result){
            return $result[0];
        } else {
            return NULL;
        }
    }

    public function get_project_by_name($project_name){
        $this->db->where('project_name', $project_name);
        $result = $this->db->get('project')->result_array();
        if ($result){
            return $result[0];
        } else {
            return NULL;
        }
    }

    public function get_all_projects(){
        return $this->db->get('project')->result_array();
    }

    /**
     * 本周是否已经发送过周报
     * @param $project_name
     * @return int
     * @throws
     */
    public function is_reported($project_name){
        $this->db->where('project_name', $project_name);
        $result = $this->db->get('project')->result();
        if (count($result) > 0){
            $r = $result[0];
            if ($r->is_reported_this_week == 1){
                return 1;
            } else if ($r->is_reported_this_week == 0){
                return 0;
            } else {
                throw new Exception("状态错误");
            }
        } else {
            throw new Exception("没有该项目");
        }
    }

    /**
     * @param $project_name
     * 设置本周本项目已经发送过周报了
     */
    public function set_has_reported($project_name){
        $this->db->set('is_reported_this_week', 1);
        $this->db->where('project_name', $project_name);
        $this->db->update('project');
    }

    /**
     *  周一凌晨自动设置所有的项目都为没有发送过周报
     */
    public function reset_reported(){
        $this->db->set('is_reported_this_week', 0);
        $this->db->update('project');
    }

}