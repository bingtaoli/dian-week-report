<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/9/19
 * Time: 23:12
 */

/**
 * Class Mod_user
 */
class Mod_user extends CI_Model {

    var $db;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $db
     * not via magic method, so pass $db
     */
    public function init($db){
        $this->db = $db;
    }

    /**
     * @param $data Array
     */
    public function add_user($data){
        /**
         * 先判断user是否已经存在
         */
        if ($this->get_user_by_name($data['username'])){
            /**
             * replace是根据主键判断， 所以改用update
             */
            $this->db->where('username', $data['username']);
            $this->db->update('user', $data);
            return;
        }
        $this->db->insert('user', $data);
    }

    public function del_user($username){
        return $this->db->delete('user', array('username' => $username));
    }

    public function get_all_user(){
        return $this->db->get('user')->result();
    }

    public function get_user_by_name($username){
        $this->db->where('username', $username);
        $result = $this->db->get('user')->result();
        if ($result){
            return $result[0];
        } else {
            return NULL;
        }
    }

    public function get_user_by_name_password($username, $password){
        $result = $this->db->get_where('user', ['username' => $username, 'password' => $password])->result();
        if ($result){
            return $result[0];
        } else {
            return NULL;
        }
    }
}