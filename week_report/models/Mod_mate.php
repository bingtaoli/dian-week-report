<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/21
 * Time: 15:30
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_mate extends CI_Model
{

    var $db;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $db
     * not via magic method, so pass $db
     */
    public function init($db)
    {
        $this->db = $db;
    }

    /**
     * @param $data
     * @throws Exception
     * 增加某个项目组中的组员
     */
    public function add_mate($data){
        /**
         * 如果已存在，则换组
         */
        try {
            if ($this->get_by_mate_name($data['mate_name'])){
                $this->db->where('mate_name', $data['mate_name']);
                $this->db->update('mate', $data);
                return;
            }
            $this->db->insert('mate', $data);
        } catch (Exception $e){
            throw new Exception($e);
        }
    }

    public function get_by_mate_name($mate_name){
        $this->db->where('mate_name', $mate_name);
        $result = $this->db->get('mate')->result();
        if ($result){
            /**
             * result()是一个数组,每个元素都是一个stdClass
             */
            return $result[0];
        } else {
            return NULL;
        }
    }

    /**
     * @param $project_name
     * @return mixed
     * 获取某个项目所有的组员
     */
    public function get_by_project_name($project_name){
        $this->db->where('project_name', $project_name);
        $result = $this->db->get('mate')->result();
        return $result;
    }

    /**
     *  在组员表中把组员删除,因为组员和项目是一一对应的，组员作为主键
     * @param matename
     * @throws
     */
    public function del_mate($mate_name){
        try {
            $this->db->where('mate_name', $mate_name);
            $this->db->delete('mate');
        } catch (Exception $e){
            throw new Exception($e);
        }
    }

    /**
     * 增加组员投入量
     * @param $mate_name
     * @param $add_devote
     * @throws
     */
    public function add_devote($mate_name, $add_devote){
        try {
            $this->db->set('devote', 'devote' . '+' . $add_devote, false);
            $this->db->where('mate_name', $mate_name);
            $this->db->update('mate');
        } catch (Exception $e){
            throw new Exception($e);
        }
    }

}