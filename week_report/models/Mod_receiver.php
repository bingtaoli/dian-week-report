<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/24
 * Time: 17:01
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_receiver extends CI_Model
{

    var $db;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $db
     * not via magic method, so pass $db
     */
    public function init($db)
    {
        $this->db = $db;
    }

    public function add_receiver($data){
        if ($this->get_receiver($data['receiver_name'])){
            /**
             * replace是根据主键判断， 所以改用update
             */
            $this->db->where('receiver_name', $data['receiver_name']);
            $this->db->update('receiver', $data);
            return;
        }
        $this->db->insert('receiver', $data);
    }

    public function del_receiver($receiver_name){
        try {
            $this->db->where('receiver_name', $receiver_name);
            $this->db->delete('receiver');
        } catch (Exception $e){
            throw new Exception($e);
        }
    }

    public function get_receiver($receiver_name){
        $this->db->where('receiver_name', $receiver_name);
        $result = $this->db->get('receiver')->result();
        if ($result){
            /**
             * result()是一个数组,每个元素都是一个stdClass
             */
            return $result[0];
        } else {
            return NULL;
        }
    }

    /**
     * 获取所有的收件人
     */
    public function get_all_receiver(){
        return $this->db->get('receiver')->result_array();
    }

}
