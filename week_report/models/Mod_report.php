<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/21
 * Time: 14:28
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_report extends CI_Model {

    var $db;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $db
     * not via magic method, so pass $db
     */
    public function init($db){
        $this->db = $db;
    }

    public function get_report_by_id($id){
        $result = $this->db->get_where('report', ['id' => $id])->result_array();
        if ($result){
            return $result[0];
        }
        return null;
    }

    /**
     * @param $data
     * @throws
     * 新增report
     */
    public function add_report($data){
        try {
            $this->db->insert('report', $data);
        } catch (Exception $e){
            throw new Exception($e);
        }
        return $this->db->insert_id();
    }

    /**
     * @param $data
     * 暂存
     */
    public function add_temp_report($data){
        /**
         * 如果已有暂存，则更新
         */
        if ($this->get_temp_report($data['fill_in_person'])){
            $this->db->where('fill_in_person', $data['fill_in_person']);
            $this->db->update('temp_report', $data);
            return;
        }
        $this->db->insert('temp_report', $data);
    }

    public function del_temp_report($project_name){
        $this->db->where('project_name', $project_name);
        $this->db->delete('temp_report');
    }

    public function get_temp_report($master_name){
        $this->db->where('fill_in_person', $master_name);
        $result = $this->db->get('temp_report')->result();
        if ($result){
            /**
             * result()是一个数组,每个元素都是一个stdClass
             */
            return $result[0];
        } else {
            return NULL;
        }
    }

    public function get_report_history($master_name, $project_name){
        return $this->db->get_where('report', ['fill_in_person' => $master_name, 'project_name' => $project_name])->result_array();
    }

    /**
     * 向on_off表中插入一条数据
     */
    public function initial_email(){
        /**
         * 查看现在有没有这条记录
         */
        $result = $this->db->get('send_on_off')->result();
        if (count($result) > 0){
            return;
        }
        $insert_data['on_off'] = 1;
        $this->db->insert('send_on_off', $insert_data);
    }

    /**
     * 邮件提醒是否已经打开
     */
    public function get_email_status(){
        $result = $this->db->get('send_on_off')->result();
        if (count($result) > 0){
            $r = $result[0];
            if ($r->on_off == 1){
                return 1;
            } else if ($r->on_off == 0){
                return 0;
            } else {
                throw new Exception("设定邮件状态错误");
            }
        } else {
            /**
             * 默认为打开邮件发送功能
             */
            return 1;
        }
    }

    /**
     * 关闭邮件通知功能
     * 考虑到节假日，不需要提醒组长填写周报，也不需要发邮件给导师
     */
    public function turn_off_email(){
        $this->db->set('on_off', 0);
        $this->db->where('on_off', 1);
        $this->db->update('send_on_off');
    }

    /**
     * 打开
     */
    public function turn_on_email(){
        $this->db->set('on_off', 1);
        $this->db->where('on_off', 0);
        $this->db->update('send_on_off');
    }
}