<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/21
 * Time: 14:25
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('week_report', TRUE);
        $this->load->model('mod_report', 'report');
        $this->report->init($this->db);
        $this->load->model('mod_receiver', 'receiver');
        $this->receiver->init($this->db);
        $this->load->model('mod_project', 'project');
        $this->project->init($this->db);
        $this->load->model('mod_mate', 'mate');
        $this->mate->init($this->db);
        $this->load->model('mod_project', 'project');
        $this->project->init($this->db);
    }

    /**
     * 周报提交
     */
    public function add_report(){
        if ($_POST){
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $master = Dianclient::get_session_client();
                if (!$master){
                    throw new IdentifyException();
                }
                $post_data = $this->input->post(NULL, true);
                $general_condition = $post_data['general_condition'] == "" ? "无" : $post_data['general_condition'];
                $delay_condition = $post_data['delay_condition'] == "" ? "无" : $post_data['delay_condition'];
                $activity_description = $post_data['activity_description'];
                $next_week_plan = $post_data['next_week_plan'];
                $week_meeting = $post_data['week_meeting'];
                $problem_track = $post_data['problem_track'];
                $project_name = $post_data['project_name'];
                $mate_comment = $post_data['mate_comment'];
                /**
                 * 如果本周已经发送过周报,则返回
                 */
                if ($this->project->is_reported($project_name)){
                    throw new Exception("本周已经发送过周报,不用再次发送哦");
                }
                $insert_data = [
                    'general_condition'   => $general_condition,
                    'delay_condition'     => $delay_condition,
                    'activity_description'  => $activity_description,
                    'next_week_plan'      => $next_week_plan,
                    'week_meeting'        => $week_meeting,
                    'problem_track'       => $problem_track,
                    'mate_comment'        => $mate_comment,
                    'remark'              => $post_data['remark'],
                ];
                $extra_data = [
                    'fill_in_person'    => $master,
                    'project_name'      => $project_name,
                    'create_time'       => date('Y-m-d'),
                ];
                $insert_data = array_merge($insert_data, $extra_data);
                /**
                 * 存储数据库
                 */
                $id = $this->report->add_report($insert_data);
                /**
                 * 为组员增加投入量
                 */
                if ($mate_comment != ''){
                    $mate_comment = explode(";", $mate_comment);
                    foreach ($mate_comment as $m) {
                        $detail = explode(',', $m);
                        $mate_name = $detail[0];
                        $add_devote = $detail[1];
                        $this->mate->add_devote($mate_name, $add_devote);
                    }
                }

                $insert_data['id'] = $id;

                /**
                 * 发送邮件
                 */
                $template_data = [
                    'report' => $insert_data,
                    'project' => $this->project->get_project_by_master($master),
                ];
                $subject = $project_name . "项目周报";
                $content = $this->load->view('email/report_email_template', $template_data, true);
                $receivers = $this->receiver->get_all_receiver();
                if (count($receivers) > 0){
                    $receiver_names_arr = [];
                    foreach ($receivers as $r){
                        array_push($receiver_names_arr, $r['email']);
                    }
                    //周报发自己一份
                    $project = $this->project->get_project_by_name($project_name);
                    $self_email = $project['project_master_email'];
                    $receiver_names_arr[] = $self_email;
                    $this->_send_mail($subject, $content, $receiver_names_arr);
                }
                /**
                 * 设置本周已经发送过周报的标志位
                 */
                $this->project->set_has_reported($project_name);
                /**
                 * 删除temp report
                 */
                $this->report->del_temp_report($project_name);
            } catch (IdentifyException $e){
                $result['status'] = 'error';
                $result['message'] = '您还没有登录';
            } catch (Exception $e){
                $result['status'] = 'error';
                $result['message'] = $e->getMessage();
            }
            /**
             * 返回结果
             */
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    public function test_remind(){
        $subject = "周报填写提醒";
        $content = $this->load->view('email/remind_template', [], true);
        $receiver_names_arr = ['1464738780@qq.com'];
        $this->_send_mail($subject, $content, $receiver_names_arr);
    }

    public function formal_remind(){
        $this->project->reset_reported();
        $subject = "周报填写提醒";
        $content = $this->load->view('email/remind_template', [], true);
        $projects = $this->project->get_all_projects();
        $master_emails = [];
        foreach ($projects as $p){
            $master_emails[] = $p['project_master_email'];
        }
        $extra_arr = [
            '1464738780@qq.com',
        ];
        $receiver_names_arr = array_merge($extra_arr, $master_emails);
        $this->_send_mail($subject, $content, $receiver_names_arr);
    }

    /**
     * @param $subject
     * @param $content
     * @param $receivers Array
     * @param bool|true $is_html
     * @throws
     * 发送邮件
     */
    public function _send_mail($subject, $content, $receivers, $is_html=true){
        $mail = $this->dianmail->get_mail();
        $mail->Subject = $subject;
        $mail->Body = $content;
        foreach ($receivers as $r){
            $mail->addAddress($r);
        }
        $mail->isHTML($is_html);
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            throw new Exception("Message has not been sent");
        }
        /**
         *  NOTE 一开始else即发送成功会echo "send success"，但是会触发ajax的error，所以删掉
         */
    }

    /**
     * 增加收件人
     */
    public function add_receiver(){
        if ($_POST){
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            /**
             * 检查是否有权限增加
             */
            $is_logged = Dianclient::login_check();
            if (!$is_logged){
                redirect('/user/login');
            }
            $identify = Dianclient::get_session_identify();
            if ($identify != 2){
                $result['status'] = 'error';
                $result['message'] = '您不是管理员账户，无法进行此操作';
            }
            try {
                $post_data = $this->input->post();
                $this->receiver->add_receiver($post_data);
            } catch (Exception $e){
                $result['message'] = $e->getMessage();
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }
    public function del_receiver(){
        if ($_POST){
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            /**
             * 检查是否有权限增加
             */
            $is_logged = Dianclient::login_check();
            if (!$is_logged){
                redirect('/user/login');
            }
            $identify = Dianclient::get_session_identify();
            if ($identify != 2){
                $result['status'] = 'error';
                $result['message'] = '您不是管理员账户，无法进行此操作';
            }
            try {
                $post_data = $this->input->post();
                $receiver_name = $post_data['receiver_name'];
                $this->receiver->del_receiver($receiver_name);
            } catch (Exception $e){
                $result['message'] = $e->getMessage();
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    /**
     * 暂存功能
     */
    public function add_temp_report(){
        if ($_POST){
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $master = Dianclient::get_session_client();
                if (!$master){
                    throw new IdentifyException();
                }
                $post_data = $this->input->post(NULL, true);
                $general_condition = $post_data['general_condition'];
                $delay_condition = $post_data['delay_condition'];
                $activity_description = $post_data['activity_description'];
                $next_week_plan = $post_data['next_week_plan'];
                $week_meeting = $post_data['week_meeting'];
                $problem_track = $post_data['problem_track'];
                $insert_data = [
                    'general_condition'   => $general_condition,
                    'delay_condition'     => $delay_condition,
                    'activity_description'  => $activity_description,
                    'next_week_plan'      => $next_week_plan,
                    'week_meeting'        => $week_meeting,
                    'problem_track'       => $problem_track,
                    'mate_comment'        => $post_data['mate_comment'],
                    'remark'              => $post_data['remark'],
                ];
                $extra_data = [
                    'fill_in_person'    => $master,
                    'project_name'      => $post_data['project_name'],
                    'create_time'       => date('Y-m-d'),
                ];
                $insert_data = array_merge($insert_data, $extra_data);
                $this->report->add_temp_report($insert_data);
            } catch (IdentifyException $e){
                $result['status'] = 'error';
                $result['message'] = '您还没有登录';
            } catch (Exception $e){
                $result['status'] = 'error';
                $result['message'] = $e->getMessage();
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    public function report_history($id){
        $report = $this->report->get_report_by_id($id);
        $this->load->view('report_history_template', ['report' => $report, 'username' => $this->username]);
    }

    public function on_email(){
        if (METHOD == 'post'){
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $this->report->turn_on_email();
            } catch (Exception $e){
                $result['status'] = 'error';
                $result['message'] = $e->getMessage();
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    public function off_email(){
        if (METHOD == 'post'){
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $this->report->turn_off_email();
            } catch (Exception $e){
                $result['status'] = 'error';
                $result['message'] = $e->getMessage();
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

}