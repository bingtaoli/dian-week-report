<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/28
 * Time: 18:15
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mate extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('week_report', TRUE);
        $this->load->model('mod_mate', 'mate');
        $this->mate->init($this->db);
    }

    public function add_mate(){
        if (METHOD == 'post'){
            $post_data = $this->input->post();
            $mate_name = $post_data['mate_name'];
            $project_name = $post_data['project_name'];
            $data = [
                'mate_name'    => $mate_name,
                'project_name' => $project_name,
                'create_time'  => date('Y-m-d'),
                'devote'       => 0,
            ];
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $this->mate->add_mate($data);
            } catch (Exception $e){
                $result['message'] = '发生未知错误，请联系管理员';
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    public function del_mate(){
        if (METHOD == 'post'){
            $post_data = $this->input->post();
            $mate_name = $post_data['mate_name'];
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $this->mate->del_mate($mate_name);
            } catch (Exception $e){
                $result['message'] = '发生未知错误，请联系管理员';
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

}