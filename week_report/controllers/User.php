<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/9/19
 * Time: 15:37
 * 显示登录界面/执行登录登出操作
 */
class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->db = $this->load->database('week_report', TRUE);
        $this->load->model('mod_user', 'user');
        $this->user->init($this->db);
        $this->load->model('mod_project', 'project');
        $this->project->init($this->db);
    }

    /**
     * 接入喻信后的登陆方式
     * 本地数据库只存root账户
     *  NOTE 测试阶段存组长作为测试用途
     */
    public function login(){
        if (METHOD == 'post'){
            $post_data = $this->input->post();
            $username = $post_data['username'];
            $password = $post_data['password'];
            $remember = $post_data['remember_me'];
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                do {
                    //1. 是否root
                    $user = $this->user->get_user_by_name_password($username, $password);
                    if ($user){
                        $result['message'] = '登陆成功';
                        Dianclient::login($username, 2, $remember);
                        break;
                    }
                    // 2.  用户名或者密码错误吗
                    $this->yuxin->login($username, $password);

                    // 3. 查找项目
                    $project = $this->project->get_project_by_master_yx($username);
                    if ($project) {
                        $result['message'] = '登陆成功';
                        Dianclient::login($project['project_master'], 1, $remember);
                        break;
                    }
                    $result = [
                        'status' => 'error',
                        'message' => '您不是项目组长或者管理员',
                    ];
                } while (0);
            } catch (IdentifyException $e){
                $result['message'] = $e->getMessage();
                $result['status'] = 'error';
            } catch (Exception $e){
                $result['message'] = '未知错误,请联系管理员';
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        } else {
            $this->load->view('login');
            return;
        }
    }


    /**
     * 登出
     */
    public function logout(){
        if (METHOD == 'post'){
            $result['status'] = 'success';
            $result['message'] = '';
            try {
                Dianclient::logout();
                if (Dianclient::get_session_identify() == 1){
                    $this->yuxin->logout();
                }
            } catch(Exception $e){
                $result['message'] = 'unknown error happens';
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }
}