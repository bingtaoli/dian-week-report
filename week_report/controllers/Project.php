<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/30
 * Time: 21:04
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('week_report', TRUE);
        $this->load->model('mod_project', 'project');
        $this->project->init($this->db);
    }

    public function add_project(){
        if (METHOD == 'post'){
            $post_data = $this->input->post();
            $extra_data = [
                'current_stage' => '项目初期',
            ];
            $data = array_merge($post_data, $extra_data);
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $this->project->add_project($data);
            } catch (Exception $e){
                $result['message'] = '发生未知错误，请联系管理员';
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    public function del_project(){
        if (METHOD == 'post'){
            $post_data = $this->input->post();
            $project_name = $post_data['project_name'];
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $this->project->del_project($project_name);
            } catch (Exception $e){
                $result['message'] = '发生未知错误，请联系管理员';
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    public function update_project(){
        if (METHOD == 'post'){
            $post_data = $this->input->post();
            $result = [
                'status' => 'success',
                'message' => '',
            ];
            try {
                $this->project->update_project($post_data);
            } catch (Exception $e){
                $result['message'] = '发生未知错误，请联系管理员';
                $result['status'] = 'error';
            }
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($result));
        }
    }

    public function reset_reported(){
        $this->project->reset_reported();
    }

}