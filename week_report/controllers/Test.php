<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/11/21
 * Time: 21:53
 * 测试添加user，project，mates
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('week_report', TRUE);
        $this->load->model('mod_report', 'report');
        $this->report->init($this->db);
        $this->load->model('mod_user', 'user');
        $this->user->init($this->db);
        $this->load->model('mod_project', 'project');
        $this->project->init($this->db);
        $this->load->model('mod_mate', 'mate');
        $this->mate->init($this->db);
        $this->load->model('mod_receiver', 'receiver');
        $this->receiver->init($this->db);
    }

    public function test_insert_project(){
        try {
            /**
             * 插入项目
             */
            $insert_project = [
                'project_name'         =>  'TEST',
                'project_master'       => '李冰涛',
                'project_description'   =>  'TEST is a project which provides another VPN way, test test test test test test test test',
                'start_time'           =>  date('Y-m-d'),
                'plan_end_time'        =>  '2015-12-1',
                'current_stage'        =>  '测试阶段',
            ];
            $this->project->add_project($insert_project);
        } catch (Exception $e){
            echo $e;
        }
    }

    public function test_insert_mate(){
        try {
            /**
             * 插入组员
             */
            $insert_mate = [
                'mate_name'   => '李超',
                'devote'      => 20,
                'create_time' => '2015-9-1',
            ];
            $this->mate->add_mate($insert_mate);
        } catch (Exception $e){
            echo $e;
        }
    }

    public function test_delete(){
        try {
            $this->user->del_user('梁进超');
        } catch (Exception $e){
            echo $e;
        }
    }

    /**
     * 测试插入user
     */
    public function test_insert_receiver(){
        $insert_data['receiver_name'] = '李冰涛';
        $insert_data['email'] = '1464738780@qq.com';
        $this->receiver->add_receiver($insert_data);
    }

    /**
     * 测试插入管理员
     */
    public function test_insert_root(){
        $insert_data['username'] = 'root';
        $insert_data['password'] = '123456';
        $insert_data['create_time'] = date('Y-m-d');
        $insert_data['identify'] = 1;
        $this->user->add_user($insert_data);
    }

    /**
     * on_off表只有一条记录
     */
    public function test_first_on_email(){
        $this->report->initial_email();
    }

    public function test_on_email(){
        $this->report->turn_on_email();
    }

    public function test_off_email(){
        $this->report->turn_off_email();
    }

    /**
     * 测试发送邮件功能
     * dianmail由于autoload了，不用手动load
     */
    public function test_send_mail(){
        $content = $this->load->view('email/report_email_template', [], true);
        $mail = $this->dianmail->get_mail();
        $mail->addAddress('15827640107@163.com');
        $mail->addAddress('1466656363@qq.com');
        $mail->isHTML(true);  // Set email format to HTML
        $mail->Subject = '测试周报邮件utf-8';
        $mail->Body = $content;
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }

    public function test_add_receiver(){
        $data = [
            'receiver_name'  => '李冰涛2',
            'email'          => "1466656363@qq.com",
        ];
        $this->receiver->add_receiver($data);
    }

    public function test_add_devote(){
        $mate_name = '何一闻';
        $add_devote = 3;
        try {
            $this->mate->add_devote($mate_name, $add_devote);
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }

    public function test_reset_reported(){
        $this->project->reset_reported();
    }

}
