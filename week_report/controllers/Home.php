<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->db = $this->load->database('week_report', TRUE);
        $this->load->model('mod_project', 'project');
        $this->project->init($this->db);
        $this->load->model('mod_mate', 'mate');
        $this->mate->init($this->db);
        $this->load->model('mod_report', 'report');
        $this->report->init($this->db);
        $this->load->model('mod_receiver', 'receiver');
        $this->receiver->init($this->db);
    }

    public function index() {
        /**
         * 必须先登录才能访问首页
         */
        $is_logged = Dianclient::login_check();
        if (!$is_logged){
            redirect('/user/login');
        }
        $identify = Dianclient::get_session_identify();
        /**
         * 根据人员类别进行分流
         */
        if ($identify == 2){
            try {
                //现在是否已经开启了
                $on_off = $this->report->get_email_status();
                $projects = $this->project->get_all_projects();
                $receivers = $this->receiver->get_all_receiver();
                $data = [
                    'username'    => $this->username,
                    'projects'    => $projects,
                    'on_off'      => $on_off,
                    'receivers'   => $receivers,
                ];
                #跳转到root页面
                $this->load->view('home_index_root', $data);
            } catch(IdentifyException $e){
                echo $e;
            } catch (Exception $e){
                echo $e->getMessage();
            }
        } else if ($identify == 1) {
            try {
                /**
                 * 获取项目信息, 包括了组员姓名
                 */
                $project = $this->project->get_project_by_master($this->username);
                if ($project == NULL){
                    throw new IdentifyException("您不是现有某个项目组的组长");
                }
                $reports = $this->report->get_report_history($this->username, $project->project_name);
                $temp_report = $this->report->get_temp_report($this->username);
                $mates = $this->mate->get_by_project_name($project->project_name);
                $data = [
                    'username'    => $this->username,
                    'project'     => $project,
                    'mates'       => $mates,
                    'temp_report' => $temp_report,
                    'reports'     => $reports,
                ];
                /**
                 * 跳转到组长页面
                 */
                $this->load->view('home_index_master', $data);
            } catch(IdentifyException $e){
                echo $e;
            } catch (Exception $e){
                echo $e->getMessage();
            }
        } else {
            redirect('/user/login');
        }
    }
}
