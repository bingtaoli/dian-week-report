<?php
/**
 * Created by PhpStorm.
 * User: bingtaoli
 * Date: 2015/9/20
 * Time: 12:35
 */

/**
 * NOTE 负责session的处理，只针对session,cookie不和数据库交互
 */

class Dianclient{

    private static $session_key_name = "HIA1HIA2HIA3";
    private static $session_key_identify = "HEHEHE";

    public static function login($username, $identify, $remember){
        $CI = & get_instance();
        $CI->load->library('session');
        $CI->session->set_userdata(self::$session_key_name, $username);
        $CI->session->set_userdata(self::$session_key_identify, $identify);
        if ($remember){
            $CI->load->helper('cookie');
            set_cookie('username', $username);
        }
        return;
    }

    public static function login_check(){
        if (self::get_session_client()){
            return true;
        }
        return false;
    }

    public static function get_session_client(){
        $CI = & get_instance();
        $CI->load->library('session');
        $username = $CI->session->userdata(self::$session_key_name);
        return $username;
    }

    public static function get_session_identify(){
        $CI = & get_instance();
        $CI->load->library('session');
        $result = $CI->session->userdata(self::$session_key_identify);
        return $result;
    }

    public static function logout(){
        $CI = & get_instance();
        $CI->load->library('session');
        $CI->session->set_userdata(self::$session_key_name, '');
        $CI->load->helper('cookie');
        delete_cookie('username');
    }
}