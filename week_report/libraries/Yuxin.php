<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/10
 * Time: 下午8:30
 */
class Yuxin {

    private $uri = "http://dian.hust.edu.cn:81/bbslogin";
    private $error_mark = "history.go(-1)";
    private $masters;
    private $master_name;
    public $session_key_cookie = "HEHECOOKIE";

    /**
     * @param $id
     * @param $pw
     * @return mixed
     * @throws Exception
     * 对外登陆API
     */
    public function login($id, $pw){
        if (!$this->check_id_pw($id, $pw)){
            throw new IdentifyException("用户名或密码错误");
        }
//        $this->get_all_master(); //得到所有的master存到本地
//        $test_data = [
//            [
//                "name" => '李冰涛',
//                "yxid" => 'libingtao'
//            ]
//        ];
//        $this->masters = array_merge($this->masters, $test_data);
//
//        if (!$this->check_master($id)){
//            throw new Exception("您不是现有项目的组长");
//        }
//        // 如果都ok,则返回这个组长的中文名,用来和我们的数据库中项目表的组长名对应
//        return $this->master_name;
    }

    /**
     * 向bbs发送logout请求
     */
    public function logout(){
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, 'http://dian.hust.edu.cn:81/bbslogout');
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_HEADER, false);
        $cookie_str =  $this->get_session_cookie();
        curl_setopt($ch2, CURLOPT_COOKIE, $cookie_str);
        $result = curl_exec($ch2);
        return;
    }

    /**
     * @param $id
     * @param $pw
     * @return bool true成功 false失败
     */
    private function check_id_pw($id, $pw){
        $data = [
            'id' => $id,
            'pw' => $pw
        ];
        $post_string = http_build_query($data, '&');
        $response = $this->post_request($this->uri, $post_string);
        /**
         * 判断是否错误
         */
        if (strpos($response, $this->error_mark)){
            return false;
        }
        $this->store_cookie($response);
        return true;
    }

    /**
     * 因为bbs的logout需要传递cookie,所以要存在本地
     */
    private function store_cookie($data){
        /**
         * 解析cookie
         */
        $reg = "/document.cookie=\'(.+)=(.+)\'/";
        preg_match_all($reg, $data, $matches);
        $keys = $matches[1];
        $values = $matches[2];
        /**
         * 组装cookie
         */
        $cookie = [];
        foreach ($keys as $i => $k){
            $cookie[$i] = $k . "=" . $values[$i];
        }
        $cookie_str = implode($cookie, ';');
        $CI = & get_instance();
        $CI->session->set_userdata($this->session_key_cookie, $cookie_str);
    }

    private function get_session_cookie(){
        $CI = & get_instance();
        $CI->load->library('session');
        $cookie = $CI->session->userdata($this->session_key_cookie);
        return $cookie;
    }

    private function post_request($remote_server, $post_string) {
        $ch = curl_init();
        $options = [
            CURLOPT_POST => 1,
            CURLOPT_URL => $remote_server,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POSTFIELDS => $post_string,
        ];
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * 团队微信的API获取所有组长
     */
    private function get_all_master(){
        $ch = curl_init(); //初始化一个CURL对象
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, "http://3222.we4mk.sinaapp.com/user/leader/");
        $json_data = curl_exec($ch);
        $data = json_decode($json_data, false);
        //var_dump($data);
        curl_close($ch);
        if ($data->result){
            $result = $data->result;
            /**
             * obj转为数组
             */
            foreach ($result as $i => $r){
                $result[$i] = (array)$r;
            }
            $this->masters = $result;
        }
    }

    /**
     * @param $id
     * @return bool
     * 判断是否是组长
     */
    private function check_master($id){
        $result = $this->masters;
        foreach ($result as $r){
            if ($r["yxid"] == $id){
                $this->master_name = $r['name'];  //把中文名存在本地
                return true;
            }
        }
        return false;
    }
}