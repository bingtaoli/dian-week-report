create database if not exists week_report character set utf8;
use week_report;

# user table, master ands root
create table if not exists user (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(10) NOT NULL,
    password varchar(20) NOT NULL ,
    identify int NOT NULL, # 身份，master or root
    create_time varchar(30) NOT NULL, # time of this user been creates, date(Y:m:d H:i:s)
    PRIMARY KEY (id)
) default charset=utf8;

#接收邮件人
create table if not exists receiver (
    id int NOT NULL AUTO_INCREMENT,
    receiver_name varchar(10) NOT NULL,
    email varchar(50) NOT NULL,
    PRIMARY KEY (id)
) default charset=utf8;

# 组员表
# 每个队员必须只能在一个项目组
# NOTE 到时候全部接入喻信
create table if not exists mate (
    id int NOT NULL AUTO_INCREMENT,
    mate_name varchar(20) unique NOT NULL,
    devote int NOT NULL, # 投入，超额|基本|不足
    create_time varchar(30) NOT NULL, # time of this mate been creates, date(Y:m:d H:i:s)
    project_name varchar(20) NOT NULL,
    PRIMARY KEY (id)
) default charset=utf8;

# project table
create table if not exists project (
    id int NOT NULL AUTO_INCREMENT,
    project_name varchar(20) NOT NULL, #项目名称
    project_number varchar(20), #项目编号
    project_master varchar(10) NOT NULL, #项目组长, NOTE 可以一个项目组多个组长?使用逗号分隔?
    project_master_yx varchar(20) NOT NULL , #组长Yuxin ID
    project_master_email varchar(50) NOT NULL , #组长email
    project_description varchar(100), # 项目描述，可以为空
    start_time varchar(30) NOT NULL, # 项目启动时间, date(Y:m:d)
    plan_end_time varchar(30) NOT NULL, # 计划结束时间, date(Y:m:d)
    current_stage varchar(10) NOT NULL, #当前所处阶段
    last_report_time varchar(30), #上次报告时间
    is_reported_this_week int default 0, # 本周是否已经报告
    PRIMARY KEY (id)
) default charset=utf8;

# report table
create table if not exists report (
    id int NOT NULL AUTO_INCREMENT,
    fill_in_person varchar(10) NOT NULL, #填表人
    project_name varchar(20) NOT NULL, # TODO 外键!
    create_time varchar(30) NOT NULL, # time of this report been creates, date(Y:m:d H:i:s)
    general_condition varchar(100) NOT NULL, #总体情况
    delay_condition varchar(100), # 延期情况
    activity_description varchar(200), # 活动描述
    next_week_plan varchar(200), # 下周计划
    week_meeting varchar(200), # 本周例会
    problem_track varchar(200), # 问题跟踪
    remark varchar(200), #备注
    mate_comment varchar(800), #组员评价
    PRIMARY KEY (id)
) default charset=utf8;

# 暂存表
create table if not exists temp_report (
    id int NOT NULL AUTO_INCREMENT,
    fill_in_person varchar(10) NOT NULL, #填表人
    project_name varchar(20) NOT NULL, # TODO 外键!
    create_time varchar(30) NOT NULL, # time of this report been creates, date(Y:m:d H:i:s)
    general_condition varchar(100) NOT NULL, #总体情况
    delay_condition varchar(100), # 延期情况
    activity_description varchar(200), # 活动描述
    next_week_plan varchar(200), # 下周计划
    week_meeting varchar(200), # 本周例会
    problem_track varchar(200), # 问题跟踪
    remark varchar(200), #备注
    mate_comment varchar(800), #组员评价
    PRIMARY KEY (id)
) default charset=utf8;

# 是否开启发送邮件通知功能
# 为了这个标志位特意建立一张表有点浪费啊，目前没有更好的解决办法
create table if not exists send_on_off (
    on_off int NOT NULL default 0
) default charset=utf8;

# on_off表必须有一条记录
insert into send_on_off values(1);