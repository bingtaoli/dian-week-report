/**
 * Created by libingtao on 15/12/12.
 */

function report_success_cb(){
    $('#report-success-modal').modal();
}
function report_fail_cb(message){
    _show_alert_message(message, 2);
}

function temp_store_success_cb(){
    $('.temp-store-result').hide();
    //  显示暂存成功
    $('#temp-store-success').show();
    setTimeout(function(){
        $('.temp-store-result').hide();
    }, 4000);
}
function temp_store_fail_cb(){
    $('.temp-store-result').hide();
    $('#temp-store-fail').show();
    setTimeout(function(){
        $('.temp-store-result').hide();
    }, 4000);
}

/**
 *  elem为glyphicon-ok按钮
 */
function add_mate_success_cb(elem){
    _show_alert_message("增加组员成功!", 1);
    /**
     * input变成<span>
     */
    var tr = $(elem).parents('tr');
    var th = $(tr).children().eq(0);
    var value = $(th).find('input').val();
    $(th).find('input').replaceWith(value);
    /**
     * 确认按钮去掉
     */
    $(elem).remove();
    /**
     * 增加del-item类
     */
    $(tr).find('.add-or-cancel').removeClass('add-or-cancel').addClass('del-item');
}
function add_mate_fail_cb(){
    _show_alert_message("增加组员失败，请联系管理员", 2);
}
/**
 *  elem为glyphicon，即按钮
 */
function del_mate_success_cb(elem){
    _show_alert_message("删除组员成功!", 1);
    // 这一行去除
    $(elem).parents('tr').remove();
}
function del_mate_fail_cb(){
    _show_alert_message("删除组员失败，请联系管理员", 2);
}

/**
 *  elem为glyphicon，即按钮
 */
function add_project_success_cb(data){
    _show_alert_message("增加项目成功", 1);
    //增加一行
    var tbody = $('#manage-project').find('tbody');
    var hidden_tr = $(tbody).find('tr:hidden');
    var clone_tr = $(hidden_tr).clone();
    set_project_information(clone_tr, data);
    $(clone_tr).show();
    $(tbody).append(clone_tr);
}
function add_project_fail_cb(){
    _show_alert_message("增加项目失败", 2);
}
/**
 *  elem为glyphicon，即按钮
 */
function del_project_success_cb(elem){
    _show_alert_message("删除项目成功!", 1);
    // 这一行去除
    $(elem).parents('tr').remove();
}
function del_project_fail_cb(){
    _show_alert_message("删除项目失败，请联系管理员", 2);
}

function update_project_success_cb(arr){
    _show_alert_message("更新项目成功!", 1);
    var tr = arr[0];
    var data = arr[1];
    set_project_information(tr, data);
}
function update_project_fail_cb(){
    _show_alert_message("更新项目失败，请联系管理员", 2);
}

function turn_on_email_success_cb(){
    _show_alert_message("打开邮件提醒成功", 1);
    window.has_on = true;
}
function turn_on_email_fail_cb(){
    _show_alert_message("打开邮件提醒成功", 2);
}
function turn_off_email_success_cb(){
    _show_alert_message("关闭邮件提醒成功", 1);
    window.has_on = false;
}
function turn_off_email_fail_cb(){
    _show_alert_message("关闭邮件提醒失败", 2);
}

function add_receiver_success_cb(elem){
    _show_alert_message("增加收件人提醒成功", 1);
    /**
     * input变成文字
     */
    var tr = $(elem).parents('tr');
    var tds = $(tr).find('td');
    for(var i=0; i<tds.length; i++){
        var input = $(tds[i]).find('input');
        $(input).replaceWith($(input).val());
    }
    // 12.21日江队发现bug,钩没有消失
    $(elem).remove();
}
function add_receiver_fail_cb(){
    _show_alert_message("增加收件人失败", 2);
}
function del_receiver_success_cb(elem){
    _show_alert_message("删除收件人成功", 1);
    // 这一行去除
    $(elem).parents('tr').remove();
}
function del_receiver_fail_cb(){
    _show_alert_message("删除收件人失败", 2);
}

function _show_alert_message(message_content, type){
    $('.alert').hide();
    var message;
    if (type == 1){
        message = $('.alert-info')[0];
    } else if (type == 2){
        message = $('.alert-warning')[0];
    } else {
        return;
    }
    var clone_message = $(message).clone();
    $(clone_message).find('.message').text(message_content);
    $('.content').prepend(clone_message);
    $(clone_message).show();
}