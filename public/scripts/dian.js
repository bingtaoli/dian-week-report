$(function () {
    _display_adjust();
    _set_listen();
});

function _set_listen() {
    $('ul.tab-items li').on('click', function () {
        var index = $(this).index();
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        var section = $('form').children().eq(index);
        $(section).removeClass('none');
        $(section).siblings().addClass('none');
    });
    /**
     * 点击出现新的一行填写项
     */
    $('.more').on('click', function(){
        /**
         * 增加项目除外,因为是弹窗,所以增加project是使用add-project-more类
         */
        var table = $(this).siblings('.table-responsive').find('table');
        var tbody = $(table).find('tbody');
        var hidden_tr = $(tbody).find('tr:hidden');
        var clone_tr = $(hidden_tr).clone();
        $(tbody).append(clone_tr);
        $(clone_tr).show();
    });
}

function _display_adjust() {
    //设置最小高度
    $('.main-content').css('min-height', window.screen.availHeight);
}

function _ajax_send(data, url, success_cb, fail_cb, success_cb_arg) {
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        timeout : 80000,  // 80s超时时间
        success: function (json) {
            if (json.status == 'success'){
                if (success_cb){
                    success_cb(success_cb_arg);
                }
            } else if (json.status == 'error'){
                if (fail_cb){
                    fail_cb(json.message);
                }
            }
        },
        error: function (e) {
            if (fail_cb){
                fail_cb(e.message);
            }
        }
    });
}

function add_pre_del_mark(elem){
    $('.pre-del').removeClass('.pre-del');
    $(elem).parents('tr').children().eq(0).addClass('pre-del');
}